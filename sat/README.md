# Scalable benchmarks for SAT solvers

This repository contains scalable benchmarks for SAT solvers, and some example solvers runtime.

## Crafted benchmarks

Proposed template

### *Benchmark name*

*Definition*

*Known theoretical properties (with references)*

*Known solvers scaling well on those benchmarks (with references)*

*Link to repository containing those benchmarks*


