# Scalable benchmarks for SAT and related problems

The repository is meant to host benchmarks to study the scalability of SAT related solvers.

By scalable, we mean benchmarks with a parameter $n$, which can be generated for values $1..20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000$.

While it has been natural in the past to use crafted benchmarks with such parameter, this repository has the aim to favor the use of scalable benchmarks for application benchmarks as well.

* [SAT](sat)
    + [Each Theory benchmarks from KTH](https://sat.csc.kth.se/cdcleval/)
    + [hard Theory benchmarks from KTH](http://www.csc.kth.se/~jakobn/publications/sat14/)
* [Pseudo-Boolean](pb)
* [MAXSAT](maxsat)
* [QBF](qbf)


The repository was initiated following the discussions that took place in [BIRS CMO 18w5208 workshop "Theory and Practice of Satisfiability Solving"](http://www.birs.ca/events/2018/5-day-workshops/18w5208).

Especially, [Moshe Vardi in his challenge talk](http://www.birs.ca/events/2018/5-day-workshops/18w5208/videos/watch/201808271841-Vardi.html) called for a study of scalable bechmarks, as an alternative to use competition benchmarks.

